#ifndef _PROVAVIEW_H_
#define _PROVAVIEW_H_

#include <QTableView>
#include <QSpinBox>
#include <QLineEdit>
#include <QItemDelegate>
namespace prova{

	class ProVaModel;
	class ManageProVaView;

	class ProVaItemDelegate 
		: public QItemDelegate
	{
		Q_OBJECT

	public:
		ProVaItemDelegate(QObject *parent = 0);

		QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
			const QModelIndex &index) const;

		void setEditorData(QWidget *editor, const QModelIndex &index) const;
		void setModelData(QWidget *editor, QAbstractItemModel *model,
			const QModelIndex &index) const;

		void updateEditorGeometry(QWidget *editor,
			const QStyleOptionViewItem &option, const QModelIndex &index) const;
	};


	class ProVaIntSpinBox
		: public QSpinBox 
	{
		Q_OBJECT
			bool isEmpty;
	public:
		ProVaIntSpinBox(QWidget *p = 0);
		bool getIsEmpty() const;
	private:
		void setIsEmpty(const bool& ie);
		public slots:
			void checkIfEmpty(const QString& text);
			void checkIfEmpty(const int& v);
	};

	class ProVaLineEdit
		: public QLineEdit 
	{
		Q_OBJECT
			bool isEmpty;
		bool value;
	public:
		ProVaLineEdit(QWidget *p = 0, bool isBoolean = false);
		bool getIsEmpty() const;
		bool getBooleanValue() const;
	private:
		void setIsEmpty(const bool& ie);
		public slots:
			void checkIfEmpty(const QString& text);
			void setBooleanValue(QString text) ;
	};


	class ProVaDoubleSpinBox
		: public QDoubleSpinBox 
	{
		Q_OBJECT
			bool isEmpty;
	public:
		ProVaDoubleSpinBox(QWidget *p = 0, bool isFloat=false);
		bool getIsEmpty() const;
	private:
		void setIsEmpty(const bool& ie);
		public slots:
			void checkIfEmpty(const QString& text);
			void checkIfEmpty(const double& v);
	};


	class NameValidator 
		: public QRegExpValidator 
	{
		Q_OBJECT
	public:
		explicit NameValidator(QObject* parent = 0);
		QValidator::State validate(QString& input, int&) const;
	};


	class ManageProVaView
		: public QTableView
	{
		Q_OBJECT
	public:
		ManageProVaView(QWidget *parent) ;
		virtual void setModel(QAbstractItemModel *model);
		QStringList getHolderList() const;
	private:
		QStringList owners;
	};

}//of namespace


#endif //_PROVAVIEW_H_
