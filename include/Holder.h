#ifndef HOLDER_H
#define HOLDER_H
#include <common.h>
#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/nvp.hpp>

using prova::HolderList;
using prova::HolderListPtr;

namespace prova
{

class Holder
{
public:
	enum HolderType{User,Group};
	typedef long HolderId;
private:
	HolderId id;
	HolderType type;
	std::string name;
	std::string createTime;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);
public:
	Holder();
	Holder(const HolderId _id, const HolderType _type, const std::string& _name, const std::string _time);
	std::string getHolderName() const;
	HolderId getHolderId() const;
	bool operator==(const Holder& other) const;

};

HolderList::const_iterator GetHolderById( const HolderList & from, Holder::HolderId id );

template<class Archive>
void prova::Holder::serialize(Archive & ar, const unsigned int version)
{
	ar & boost::serialization::make_nvp("ID", id);
	ar & boost::serialization::make_nvp("type",type);
	ar & boost::serialization::make_nvp("name",name);
	ar & boost::serialization::make_nvp("create",createTime);
}

} //namespace prova
#endif //HOLDER_H
