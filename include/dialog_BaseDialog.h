#ifndef _BASEDIALOG_H
#define _BASEDIALOG_H

#include <ui_base_dialog.h>


#include <QDialog>

namespace prova {
/* each dialog consists of the same design: content widget and buttons-bar
* additionally it can consist of info line
*/
class BaseDialog: public QDialog 
{

	Q_OBJECT

	public:
		enum ButtonDesign {NoButtons, OkCancel, Close, YesNo, Apply};

		BaseDialog(QWidget *parent, ButtonDesign bd = BaseDialog::OkCancel);
		~BaseDialog();

		void SetContentWidget(QWidget * widget);
		QWidget * getContent();

		///@name dialog settings
		//@{
		void setAllowResizing( bool allowResizing );
		void setInfoLineText( const QString & text);
		void setButtonDesign(ButtonDesign bd);
		bool allowResizing() const;

		void setAvoidCloseOnEnter( bool avoidClosing );
		bool getAvoidCloseOnEnter() const;
		//@}

	protected:
		QPushButton * AddApplyButton(const QObject * callback_receiver, const char * callback_method);

		///overload of QDialog
		virtual void keyPressEvent( QKeyEvent * e );

	private:
		Ui::dialog_BaseDialog ui;
		ButtonDesign buttons;
	protected:
		QWidget * contentWidget;

		bool avoidCloseOnEnter;
};


}//of namespace prova

#endif //_BASEDIALOG_H
