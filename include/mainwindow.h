#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <common.h>
#include <QMainWindow>
#include <QStandardItemModel>
#include <QMenu>
#include <QCloseEvent>


namespace Ui {
class MainWindow;
}

namespace prova {
class ManagePropertyDescriptionsDialog;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const std::string datafile = "C:\filename.xml", QWidget *parent = 0);
    ~MainWindow();
	void saveModel(std::ofstream& ss, prova::ProVaModelPtr model);
	void loadModel(std::ifstream& ss, prova::ProVaModelPtr& m);
private:
	// property/value model
	prova::ProVaModelPtr m;
	// index of item contextmenue is shown for 
	QModelIndex actualidx;
    Ui::MainWindow *ui;
	QMenu* contextMenu;
	QStandardItemModel* model;
	std::string datafile;
	prova::ManagePropertyDescriptionsDialog* dlg;
	std::vector<prova::TreeElementPtr> treeitems;
	virtual void closeEvent(QCloseEvent *);
	//create tree based on the input data
	void CreateTree();

public slots:
	void StartManageProperties();
	void showContextMenu(const QPoint &);
	void showEditPropertyValueDialog();

};

#endif // MAINWINDOW_H
