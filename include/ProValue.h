#ifndef PROVALUE_H
#define PROVALUE_H

#include <common.h>

#include <boost/variant.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>

using namespace std;

namespace prova {

class ProValue
	// inherit from boost::static_visitor for using with apply_visitor
	// goal: convertiond of boost::variant to string depending on datatype
	: public boost::static_visitor<std::string>
{
public:
	typedef boost::variant<bool, int, long, float, double, std::string> PVALUE;
	template <typename T> 
	string operator()( T & t) const
	{
		stringstream str;
		str << t;
		return str.str();
	}
	// float and double should be converted to string by using of different precision
	string operator()( double & t) const
	{
		stringstream str;
		str << setprecision(DBL_DIG) << t;
		return str.str();
	}
	string operator()( float & t) const
	{
		stringstream str;
		str << setprecision(FLT_DIG) << t;
		return str.str();
	}
private:
	PVALUE value; 
	//desctiption that belongs to the value
	ProDescriptionPtr desc;
	// item tree the value is dedicated
	TreeElementPtr linkedElement;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);
public:
	ProValue();
	ProValue(const ProValue& other);
	ProValue(PVALUE _value, ProDescriptionPtr _desc, boost::shared_ptr<TreeElement> _element);
	PVALUE getValue() const;
	DType getDType() const;
	ProDescriptionPtr getProDescription() const;
	boost::shared_ptr<TreeElement> getTreeElement() const;
	void setValue(PVALUE newvalue);
	ID getDescId() const;
	long getTreeElementId() const;
	~ProValue();
};

template<class Archive>
void prova::ProValue::serialize(Archive & ar, const unsigned int version)
{
	ar & value;
	ar & boost::serialization::make_nvp("PropertyDescription",desc);
	ar & boost::serialization::make_nvp("LinkedTreeItem",linkedElement);
}

}// namespace prova

#endif // PROVALUE_H
