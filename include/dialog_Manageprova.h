#ifndef DIALOGADDPROPERTYDESCRIPTION_H
#define DIALOGADDPROPERTYDESCRIPTION_H

#include <common.h>

#include <ui_widget_manage_prova.h>
#include <dialog_BaseDialog.h>
#include <QObject>
#include <QTableView>
#include <QSortFilterProxyModel>

namespace prova{
		class ProVaModel;
		/*
		* Use the same wirget to manage property descriptions as well as
		* property values. If widget is created with a pointer to a tree item 
		* it is used to manage values, otherwise to manage descriptions
		*/
		class ManagePropertyDescriptionsWidget
			: public QWidget 
		{
			Q_OBJECT
		public:
			///Manage Property Descriptions C'tor
			ManagePropertyDescriptionsWidget(ProVaModelPtr _model, QWidget *parent = 0, 
				TreeElementPtr le = TreeElementPtr());
			///When property description was created not completely
			///remove it after closing of dialog automatically
			void removeEmptyPropertyDescriptions();

			void setTreeElement(const TreeElementPtr le);
			void UpdateView();
			virtual ~ManagePropertyDescriptionsWidget();
		private:
			/// all data about properties and values
			ProVaModelPtr model;
			/** Holds the GUI elements.	*/
			Ui::widget_manage_prova ui;

			public slots:	
				void CreateNewPropDesc();
				void RemoveItem();
				void ShowEmptyValues();
				void ShowInheritedValues();
				void OnItemClicked();
		};


		class ManagePropertyDescriptionsDialog
			: public BaseDialog 
		{

			Q_OBJECT

		public:
			ManagePropertyDescriptionsDialog(QWidget *parent, ProVaModelPtr model
				,TreeElementPtr le = boost::shared_ptr<TreeElement>());
			void setTreeElement(const TreeElementPtr le);

		};

}

#endif
