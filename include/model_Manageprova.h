#ifndef _model_ProVaModel_h__
#define _model_ProVaModel_h__
#include <common.h>
#include <QSortFilterProxyModel>
#include <QVariant>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/nvp.hpp>


namespace prova {
	
class ProVaItem
{
public:
	enum State {Prepared, Stored};
	ProVaItem();
	ProVaItem(ProDescriptionPtr _prodesc, 
		ProValuePtr _prova = ProValuePtr(),
		const bool& _isParentValue = false);
	ProVaItem(const ProVaItem& other);

	std::string getName() const;
	std::string getDatatype() const;
	QVariant getValue(int role) const;
	State getState() const;
	bool getIsParentValue() const;
	std::string getHolder() const;
	int getHolderId() const;

	ProDescriptionPtr getDescription() const;
	ProValuePtr getPropValue() const;
	void setName(const std::string& _name);
	void setDatatype(const std::string& _dt);
	void setHolder(const HolderPtr _owner);
	bool operator==(const ProVaItem& other) const;
	ProVaItem& operator=( const ProVaItem & other );
private:
	std::string propertyname;
	std::string propertydatatype;
	HolderPtr owner;
	ProDescriptionPtr prodesc;
	ProValuePtr prova;
	State state;
	bool isParentValue;
};



class ProVaModel :
	public QAbstractTableModel
{
	Q_OBJECT
public:
	enum ManageKind {Properties, Values};
	ProVaModel();
	ProVaModel(HolderListPtr _holders, ProDescriptionListPtr _descriptions, const ManageKind& _mkind = ProVaModel::Properties, QObject *parent=0);
	~ProVaModel(void);

	int rowCount(const QModelIndex &parent) const;
	int columnCount(const QModelIndex &parent) const;
	QVariant data(const QModelIndex &index, int role) const;
	QVariant headerData(int section, Qt::Orientation orientation, int role) const;

	bool setData(const QModelIndex &index, const QVariant &value, int role=Qt::EditRole);
	bool insertRows(int position, int rows, const QModelIndex &index=QModelIndex());
	bool removeRows(int position, int rows, const QModelIndex &index=QModelIndex());
	void removeEmptyItems();
	void clearPropertyValues();
	
	void setTreeElement(const TreeElementPtr le);
	void setManageKind(const ManageKind& _kind);
	ProVaModel::ManageKind getManageKind() const;
	void resetProVaModel();
	HolderListPtr getHolderList() const;

	std::string getItemDatatype(const QModelIndex &index) const;
	std::string getItemName(const QModelIndex &index) const;
	std::string getItemHolder(const QModelIndex &index) const;
	bool getItemIsEditable(const QModelIndex &index) const;
	bool isEmptyItem(const QModelIndex &index) const;
	bool itemHasAssignedValue(const int &row) const;
	virtual Qt::ItemFlags flags(const QModelIndex &index) const;

private:
	ManageKind modelkind;
	TreeElementPtr linkedelement;
	ProDescriptionListPtr descriptions;
	HolderListPtr ownerlist;

	std::vector<ProVaItemPtr> prova_table;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);
};

template<class Archive>
void prova::ProVaModel::serialize(Archive & ar, const unsigned int version)
{
	ar & boost::serialization::make_nvp("Holders",ownerlist);
	ar & boost::serialization::make_nvp("Descriptions",descriptions);
}

class ProVaViewFilterProxyModel
	: public QSortFilterProxyModel
{
	Q_OBJECT

	bool showEmptyValues;
	bool showInheritedValues;
public:
	ProVaViewFilterProxyModel(QWidget *parent, bool _showEmptyValues = false, bool _showInheritedValues = false) ;
	void setShowEmptyValues(const bool& _showEmptyValues);
	void setShowInheritedValues(const bool& _showInheritedValues);
	virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
	void removeEmptyPropertyDescriptions();
};

} //namespace prova

#endif // _model_ProVaModel_h__