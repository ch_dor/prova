#ifndef COMMON_H
#define COMMON_H

#include <vector>
#include <boost/shared_ptr.hpp>

namespace prova
{
// common forward declarations
	class Holder;
	class ProDescription;
	class TreeElement;
	class ProValue;
	class ProVaItem;
	class ProVaModel;
	class ProVaViewFilterProxyModel;

	enum DType {
		Bool,
		Int,
		Long,
		Float, 
		Double, 
		Text,
		Binary,
		Unkown
	} ;

//common typedefs
// use boost::shared_ptr and stl containers to manage data
typedef long ID;

typedef boost::shared_ptr<Holder> HolderPtr;
typedef std::vector<HolderPtr> HolderList;
typedef boost::shared_ptr<HolderList> HolderListPtr;


typedef boost::shared_ptr<ProDescription> ProDescriptionPtr;
typedef std::vector<ProDescriptionPtr> ProDescriptionList;
typedef boost::shared_ptr<ProDescriptionList> ProDescriptionListPtr;

typedef boost::shared_ptr<TreeElement> TreeElementPtr;

typedef boost::shared_ptr<ProValue> ProValuePtr;
typedef std::vector<ProValuePtr> ProValueList;
typedef boost::shared_ptr<ProValueList> ProValueListPtr;

typedef boost::shared_ptr<ProVaItem> ProVaItemPtr;

typedef boost::shared_ptr<ProVaModel> ProVaModelPtr;
}// namespace prova

#endif