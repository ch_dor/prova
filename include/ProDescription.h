#ifndef PRODESC_H
#define PRODESC_H
#include <common.h>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <string>
#include <vector>

namespace prova
{

class ProDescription
{

private:
	DType dtype;
	std::string propertyname;
	HolderPtr owner;
	ID propertyid;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & boost::serialization::make_nvp("Propertyid",propertyid);
		ar & boost::serialization::make_nvp("Datatype",dtype);
		ar & boost::serialization::make_nvp("Propertyname",propertyname);
		ar & boost::serialization::make_nvp("Holder",owner);
	}

public:
	ProDescription(void);
	ProDescription(const std::string& _name, DType _dtype, HolderPtr _owner, ID _id=-1);
	ProDescription(const ProDescription& other);
	DType getDType() const;
	std::string getDTypeAsString() const;
	ID getPropertryID() const;
	HolderPtr getHolder() const;
	static std::string PropertyType2Str( const DType & type ) ;
	static DType PropertyType2DType( const std::string  & type ) ;
	void setPropertyID(ID _id);
	std::string getPropertyName() const;
	void setPropertyName(std::string& newname);
	~ProDescription(void);
	bool operator ==(const ProDescription& other) const;
};
}
#endif // PRODESC_H
