#ifndef TREEELEMENT_H
#define TREEELEMENT_H

#include <QStandardItem>
#include <common.h>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/shared_ptr.hpp>

namespace prova {

class TreeElement
{
public:
	typedef long LinkId;
private:
	LinkId id;
	int level;
	std::string label;
	prova::ID holderId;
	prova::ProValueListPtr assignedvalues;
	prova::ID parentId;

	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version);

public:
	prova::TreeElement::LinkId getId() const;
	prova::ID getHolderId() const;
	prova::ID getParentId() const;
	int getLevel() const;
	std::string getLabel() const;
	TreeElement();
	TreeElement(int _id, int _level, std::string& _label, int _holderId, prova::ID _parentID = -1);
	~TreeElement(void);
};

template<class Archive>
void prova::TreeElement::serialize(Archive & ar, const unsigned int version)
{
	ar & boost::serialization::make_nvp("TreeItemId",id);
	ar & boost::serialization::make_nvp("TreeItemLevel",level);
	ar & boost::serialization::make_nvp("TreeItemLabel",label);
	ar & boost::serialization::make_nvp("TreeItemParentId",parentId);
	ar & boost::serialization::make_nvp("HolderId",holderId);
}

}

#endif

