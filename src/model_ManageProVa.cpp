#include <common.h>
#include <Holder.h>
#include <TreeElement.h>
#include <model_ManageProVa.h>
#include <ProDescription.h>
#include <ProValue.h>
#include <QString>
#include <QMessageBox>
#include <QToolTip>

#include <boost/variant.hpp>
#include <boost/make_shared.hpp>
#include <boost/foreach.hpp>

namespace prova {


ProVaItem::ProVaItem(ProDescriptionPtr _prodesc, 
	ProValuePtr _prova /*= ProValuePtr()*/,
	const bool& _isParentValue/* = false*/)
	:prodesc(_prodesc),prova(_prova),isParentValue(_isParentValue)
{
	state = ProVaItem::Stored;
}

ProVaItem::ProVaItem(const ProVaItem& other)
{
	prodesc = other.prodesc;
	prova = other.prova;
	state = other.state;
	isParentValue = other.isParentValue;

}

ProVaItem::ProVaItem()
{
	state = ProVaItem::Prepared;
	isParentValue = false;
}

std::string ProVaItem::getName() const
{
	if(prodesc)
		return prodesc->getPropertyName();
	if(!propertyname.empty())
		return propertyname;
	return "";
}

std::string ProVaItem::getDatatype() const
{
	if(prodesc)
		return prodesc->getDTypeAsString();
	if(!propertydatatype.empty())
		return propertydatatype;
	return "";
}

QVariant ProVaItem::getValue(int role) const
{
	if(prodesc)
	{
		switch(prodesc->getDType())
		{
			case Int:
				if(prova)
				{
					return QVariant(boost::get<int>(prova->getValue()));
				}
				else if(!prova && role == Qt::EditRole)
				{
					return QVariant(0);
				}
			case Bool:
				if(prova)
				{
					return QVariant(boost::get<bool>(prova->getValue()));
				}
				else if(!prova && role == Qt::EditRole)
				{
					return QVariant(true);
				}
			case Float:
				if(prova)
				{
					return QVariant(boost::get<float>(prova->getValue()));
				}
				else if(!prova && role == Qt::EditRole)
				{
					return QVariant(0.0);
				}
			case Double:
				if(prova)
				{
					return QVariant(boost::get<double>(prova->getValue()));
				}
				else if(!prova && role == Qt::EditRole)
				{
					return QVariant(0.0);
				}
			case Text:
				if(prova)
				{
					return QVariant((boost::get<std::string>(prova->getValue())).c_str());
				}
				else if(!prova && role == Qt::EditRole)
				{
					return QVariant(" ");
				}
			case Unkown:
				return QVariant();
		}
	}
	return QVariant();
}


bool ProVaItem::operator==(const ProVaItem& other) const
{
	if(*prodesc == *other.prodesc)
		return true;
	else
		return false;
}

ProVaItem& ProVaItem::operator=(const ProVaItem & other)
{
	if (this != &other) 
	{
		prodesc = other.prodesc;
		prova = other.prova;
	}
	return *this;
}

void ProVaItem::setName(const std::string& _name)
{
	propertyname = _name;
}

ProVaItem::State ProVaItem::getState() const
{
	return state;
}

void ProVaItem::setDatatype(const std::string& _dt)
{
	if(state == ProVaItem::Prepared)
		propertydatatype = _dt;
}
ProDescriptionPtr ProVaItem::getDescription() const
{
	return prodesc;
}

ProValuePtr ProVaItem::getPropValue() const
{
	return this->prova;
}

bool ProVaItem::getIsParentValue() const
{
	return isParentValue;
}

std::string ProVaItem::getHolder() const
{
	if(prodesc)
		return prodesc->getHolder()->getHolderName();
	if(owner)
		return owner->getHolderName();
	return "";
}

void ProVaItem::setHolder(const HolderPtr _owner)
{
	owner = _owner;
}

int ProVaItem::getHolderId() const
{
	if(prodesc)
		return prodesc->getHolder()->getHolderId();
	if(owner)
		return owner->getHolderId();
	return -1;
}


/*----------------------------------------------------------------------------------------------------------*/
ProVaModel::ProVaModel()
	: QAbstractTableModel(0)
	, modelkind(ProVaModel::Properties)
{
}

ProVaModel::ProVaModel(HolderListPtr _holders, ProDescriptionListPtr _descriptions, const ProVaModel::ManageKind& _mkind, QObject *parent/*=0*/)
	: QAbstractTableModel(parent)
	,ownerlist(_holders)
	, modelkind(_mkind)
	,descriptions(_descriptions)
{
	BOOST_FOREACH(ProDescriptionPtr desc, *descriptions)
	{
		prova_table.push_back(boost::make_shared<ProVaItem>(desc));
	}
}


ProVaModel::~ProVaModel(void)
{
}

int ProVaModel::rowCount(const QModelIndex &parent) const
{
	return (int)prova_table.size();
}

int ProVaModel::columnCount(const QModelIndex &parent) const
{
	return 3;
}

QVariant ProVaModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();
	
	if (role == Qt::DisplayRole || role == Qt::EditRole  ) 
	{
		switch(index.column())
		{
		case 0:
			return QString(prova_table[index.row()]->getName().c_str());
			break;
		case 1:
			return QString(prova_table[index.row()]->getDatatype().c_str());
			break;
		case 2:
			if(modelkind == ProVaModel::Values)
				return prova_table[index.row()]->getValue(role);
			else
				return QVariant(prova_table[index.row()]->getHolder().c_str());
			break;
		default:
			return QVariant();
		}
	}
	if (role == Qt::BackgroundColorRole && index.column() == 2 
		&& modelkind == ProVaModel::Values 
		&& !prova_table[index.row()]->getPropValue())
	{
		return QColor(Qt::gray);
	}
	if (role == Qt::FontRole && prova_table[index.row()]->getState()==ProVaItem::Prepared)
	{
		QFont theFont;
		theFont.setItalic(true);
		return  QVariant::fromValue(theFont);
	}
	if (role == Qt::ForegroundRole  && prova_table[index.row()]->getIsParentValue())
	{
		return QVariant::fromValue(QColor(Qt::gray));
	}
	if( role == Qt::ToolTipRole )
	{
		if(prova_table[index.row()]->getIsParentValue())
		{
			QString linkedelementname(prova_table[index.row()]->getPropValue()->getTreeElement()->getLabel().c_str());
			QString linkedelementowner(prova_table[index.row()]->getHolder().c_str());
			return QString("Value of parent item: %1\nHolder: %2 ").arg(linkedelementname,linkedelementowner);
		}
		else
		{
			QToolTip::hideText();
			return QVariant();
		}
	}
	
	return QVariant();//AbstractTableModel::data(index, role);
}

QVariant ProVaModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch (section) {
		case 0:
			return tr("Name");

		case 1:
			return tr("Datatype");

		case 2:
			if(modelkind == ProVaModel::Values)
				return tr("Value");
			else
				return tr("Holder");
		default:
			return QVariant();
		}
	}
	return QVariant();
}

bool ProVaModel::setData(const QModelIndex &index, const QVariant &value, int role/*=Qt::EditRole*/)
{
	if (index.isValid() && role == Qt::EditRole) 
	{
		int row = index.row();
		if (index.column() == 0 && modelkind == ProVaModel::Properties) 
		{//name or rename property
			std::string newname(value.toString().toLocal8Bit().data());
			ProDescriptionPtr tmp = prova_table[row]->getDescription();
			if(tmp && !newname.empty() && tmp->getPropertyName()!=newname)
			{//rename
				for(ProDescriptionList::iterator p =descriptions->begin();p != descriptions->end();++p)
				{
					if(*(*p)==*tmp)
					{	
						(*p)->setPropertyName(newname);
					//	network->RenameMoMaPropertyDesc(tmp->getPropertryID(),newname);
						//tmp->setMoMaPropertyName(value.toString().toLocal8Bit().data());
						//prova_table[row].reset(new ProVaItem(tmp));
						//descriptions->push_back(tmp);
						return true;
					}
				}
				throw std::exception("Property description can not be found");
			}
			else
			{//name it
				prova_table[row]->setName(value.toString().toLocal8Bit().data());
			}
		}
		else if (index.column() == 1)
		{//set datatype
			prova_table[row]->setDatatype(value.toString().toLocal8Bit().data());
		}
		else if (index.column() == 2 && modelkind == ProVaModel::Values)
		{// create or drop value
			if(!value.isNull())
			{//create or change value
				ProValue::PVALUE pv;
				switch(	ProDescription::PropertyType2DType(prova_table[row]->getDatatype()))
				{
				case Int:
					pv = value.toInt();
					break;
				case Bool:
					pv = value.toBool();
					break;
				case Float:
					pv = value.toFloat();
					break;
				case Double:
					pv = value.toDouble();
					break;
				case Text:
					pv = std::string(value.toString().toLocal8Bit().data());
					break;
				case Unkown:
				default:
					pv = "";
				}
				//renew value
				if(prova_table[row]->getPropValue() && !(prova_table[row]->getPropValue()->getValue()==pv))
				{
					ProValuePtr prova = 
						boost::shared_ptr<ProValue>(new ProValue(pv,prova_table[row]->getDescription(),linkedelement));
					prova_table[row].reset(new ProVaItem(prova_table[row]->getDescription(),prova));
				}
				// add new value
				if(!prova_table[row]->getPropValue())
				{
					ProValuePtr prova =
						boost::shared_ptr<ProValue>(new ProValue(pv,prova_table[row]->getDescription(),linkedelement));
					prova_table[row].reset(new ProVaItem(prova_table[row]->getDescription(),prova));
				}
			}
			else //value.isNull(), drop value if exists
			{
				if(prova_table[row]->getPropValue())
				{
					prova_table[row].reset(new ProVaItem(prova_table[row]->getDescription()));
				}
			}
		}
		else if (index.column() == 2 && modelkind == ProVaModel::Properties)
		{//manage owner
			HolderPtr newowner = (*ownerlist)[value.toInt()];
			ProDescriptionPtr tmp = prova_table[row]->getDescription();
			if(tmp && newowner && prova_table[row]->getHolderId()!=newowner->getHolderId())
			{ // change owner
				for(ProDescriptionList::iterator p =descriptions->begin();p != descriptions->end();++p)
				{
					if(*(*p)==*tmp)
					{
						descriptions->erase(p);
						ProDescriptionPtr newdesc(new ProDescription(tmp->getPropertyName(),tmp->getDType(),newowner,tmp->getPropertryID()));
						descriptions->push_back(newdesc);
						prova_table[row].reset(new ProVaItem(newdesc));
						return true;
					}
				}
				throw std::exception("Property description can not be found");
			}
			else
			{//create owner in an empty item
				prova_table[row]->setHolder(newowner);
			}
		}

		// create new property if all three fields consists of a value 
		if(prova_table[row]->getState()==ProVaItem::Prepared)
		{
			if(!prova_table[row]->getName().empty() && !prova_table[row]->getDatatype().empty() && prova_table[row]->getHolderId()!=-1)
			{
				DType dt = ProDescription::PropertyType2DType(prova_table[row]->getDatatype());
				ProDescriptionPtr prodesc;
				BOOST_FOREACH(HolderPtr owner, *ownerlist)
				{
					if(owner->getHolderId()==prova_table[row]->getHolderId())
					{
						prodesc = boost::shared_ptr<ProDescription>(new ProDescription(prova_table[row]->getName(),dt,owner));
						break;
					}
				}
				descriptions->push_back(prodesc);
				prova_table[row].reset(new ProVaItem(prodesc));
			}
		}
	
		emit(dataChanged(index, index));

		return true;
	}

	return false;
}

bool ProVaModel::insertRows(int position, int rows, const QModelIndex &index/*=QModelIndex()*/)
{
	if(modelkind == ProVaModel::Properties)
	{
		beginInsertRows(QModelIndex(), position, position+rows-1);
		prova_table.push_back(boost::make_shared<ProVaItem>());
		endInsertRows();
		return true;
	}
	return false;
}

bool ProVaModel::removeRows(int position, int rows, const QModelIndex &index/*=QModelIndex()*/)
{
	beginRemoveRows(QModelIndex(), position, position+rows-1);
	if(modelkind == ProVaModel::Properties)
	{
		if(prova_table[position]->getDescription())
		{
			for(std::vector<ProDescriptionPtr>::const_iterator pos = descriptions->begin();pos!=descriptions->end();++pos)
			{
				if(*(*pos)==*prova_table[position]->getDescription())
				{
					descriptions->erase(pos);
					break;
				}
			}
			prova_table.erase(prova_table.begin()+position);
		}
		else
		{
			prova_table.erase(prova_table.begin()+position);
		}
	}
	else if(modelkind == ProVaModel::Values && prova_table[position]->getDescription() && prova_table[position]->getPropValue())
	{
		prova_table[position].reset(new ProVaItem(prova_table[position]->getDescription()));
	}
	endRemoveRows();
	return true;	
}

void ProVaModel::setTreeElement(const TreeElementPtr le)
{
	setManageKind(ProVaModel::Values);
	linkedelement = le;
	ProValueListPtr values = boost::shared_ptr<ProValueList>(new ProValueList());

	int oldrownumber=prova_table.size();
	prova_table.clear();
	BOOST_FOREACH(ProDescriptionPtr desc, *descriptions)
	{
		bool found = false;
		for(int i = 0;i<(int)values->size();++i)
		{
			if(*desc == *(*values)[i]->getProDescription())
			{
				if((*values)[i]->getTreeElement()->getId() == le->getId())// own value
				{
					found = true;
					boost::shared_ptr<ProVaItem> tmp(new ProVaItem(desc,(*values)[i]));
					prova_table.push_back(tmp);
				}
				else
				{
					boost::shared_ptr<ProVaItem> tmp(new ProVaItem(desc,(*values)[i],true));
					prova_table.push_back(tmp);
				}
				values->erase(values->begin()+i);
				i--;
			}
		}
		if(!found)
		{
			prova_table.push_back(boost::make_shared<ProVaItem>(desc));
		}
	}
}

void ProVaModel::resetProVaModel()
{
	descriptions = boost::shared_ptr<ProDescriptionList>();
	ownerlist = boost::shared_ptr<HolderList>();
	BOOST_FOREACH(ProDescriptionPtr desc, *descriptions)
	{
		prova_table.push_back(boost::make_shared<ProVaItem>(desc));
	}
	linkedelement = TreeElementPtr();

}

Qt::ItemFlags ProVaModel::flags(const QModelIndex &index) const
{
	if(!index.isValid())
		return Qt::ItemIsEnabled;
	else
	{
		Qt::ItemFlags flags = QAbstractTableModel::flags(index);
		if(index.column()==0 && modelkind==ProVaModel::Properties) // name
		{
			return flags | Qt::ItemIsEditable;
		}
		if(index.column()==1 && prova_table[index.row()]->getState()==ProVaItem::Prepared) // datatype && new description
		{
			return flags | Qt::ItemIsEditable;
		}
		if(index.column()==2 ) // if properties, owner editable, if values, value editable
		{
			return flags | Qt::ItemIsEditable;
		}
		return flags;
	}
}

std::string ProVaModel::getItemDatatype(const QModelIndex &index) const
{
	return prova_table[index.row()]->getDatatype();
}

std::string ProVaModel::getItemName(const QModelIndex &index) const
{
	return prova_table[index.row()]->getName();
}

void ProVaModel::removeEmptyItems()
{
	for(int i=0;i<(int)prova_table.size();++i)
	{
		if(prova_table[i]->getState()==ProVaItem::Prepared)
		{
			prova_table.erase(prova_table.begin()+i);
			i-=1;
		}
	}
}

bool ProVaModel::itemHasAssignedValue(const int& row) const
{
	if(row>-1 && row < (int)prova_table.size())
		return (prova_table[row]->getPropValue() || prova_table[row]->getState()==ProVaItem::Prepared);
	return false;
}

bool ProVaModel::isEmptyItem(const QModelIndex &index) const
{
	if(prova_table[index.row()]->getState()==ProVaItem::Prepared)
		return true;
	return false;
}

void ProVaModel::clearPropertyValues()
{	
	prova_table.clear();
	BOOST_FOREACH(ProDescriptionPtr desc, *descriptions)
	{
		prova_table.push_back(boost::make_shared<ProVaItem>(desc));
	}
}

ProVaModel::ManageKind ProVaModel::getManageKind() const
{
	return modelkind;
}

void ProVaModel::setManageKind(const ProVaModel::ManageKind& _kind)
{
	modelkind = _kind;
}

std::string ProVaModel::getItemHolder(const QModelIndex &index) const
{
	return prova_table[index.row()]->getHolder();
}

bool ProVaModel::getItemIsEditable(const QModelIndex &index) const
{
	return !prova_table[index.row()]->getIsParentValue();
}

HolderListPtr ProVaModel::getHolderList() const
{
	return ownerlist;
}


/*----------------------------------------------------------------------------------------------------------------------*/


bool ProVaViewFilterProxyModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
	ProVaModel* model = (ProVaModel*)sourceModel();
	if(!model){
		return false;
	}
	if(model->getManageKind()==ProVaModel::Properties){
		return true;
	}
	if(!showEmptyValues && !model->itemHasAssignedValue(source_row)){
		return false;
	}
	if(!showInheritedValues && !model->getItemIsEditable(model->index(source_row,0))){
		return false;
	}
	return true;
}

ProVaViewFilterProxyModel::ProVaViewFilterProxyModel(QWidget *parent, bool _showEmptyValues /*= false*/, bool _showInheritedValues /*= false*/)
	: QSortFilterProxyModel(parent),showEmptyValues(_showEmptyValues),showInheritedValues(_showInheritedValues)
{
	setSortCaseSensitivity(Qt::CaseInsensitive);
}

void ProVaViewFilterProxyModel::removeEmptyPropertyDescriptions()
{
	ProVaModel* model = (ProVaModel*)sourceModel();
	model->removeEmptyItems();
}

void ProVaViewFilterProxyModel::setShowEmptyValues(const bool& _showEmptyValues)
{
	showEmptyValues = _showEmptyValues;
}

void ProVaViewFilterProxyModel::setShowInheritedValues(const bool& _showInheritedValues)
{
	showInheritedValues = _showInheritedValues;
}

}