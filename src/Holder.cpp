#include <common.h>
#include <Holder.h>

namespace prova {

Holder::Holder(const HolderId _id, const HolderType _type, const std::string& _name, const std::string _time)
	:id(_id),type(_type),name(_name),createTime(_time)
{

}

Holder::Holder()
	:id(-1),type(Holder::User),name(""),createTime("")
{

}

std::string Holder::getHolderName() const
{
	return name;
}

Holder::HolderId Holder::getHolderId() const
{
	return id;
}

bool Holder::operator==(const Holder& other) const
{
	if(id!=other.id)
		return false;
	if(type!=other.type)
		return false;
	if(name!=other.name)
		return false;
	if(createTime!=other.createTime)
		return false;
	return true;
}


prova::HolderList::const_iterator GetHolderById( const prova::HolderList & from, Holder::HolderId id )
{
	HolderList::const_iterator result = from.begin();
	for(; result != from.end(); ++result)
	{
		if( (*result)->getHolderId() == id)
			break;
	}

	return result;
}

}