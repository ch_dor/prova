#include <dialog_BaseDialog.h>

#include <QGridLayout>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QKeyEvent>

namespace prova{

	BaseDialog::BaseDialog( QWidget *parent, ButtonDesign bd /*= BaseDialog::OkCancel*/) 
		: QDialog(parent)
		, contentWidget(NULL)
	{
		ui.setupUi(this);
		setButtonDesign(bd);
		ui.textEdit_infoline->setVisible(false);
		ui.textEdit_infoline->setTextInteractionFlags(Qt::NoTextInteraction);
		//by default resizing is allowed
		this->setAllowResizing(true);
	}
	//------------------------------------------------------------------------------
	BaseDialog::~BaseDialog()
	{
		if(contentWidget) 
			delete contentWidget;
	}
	//------------------------------------------------------------------------------

	void BaseDialog::setButtonDesign(ButtonDesign bd)
	{
		if(bd!=BaseDialog::OkCancel)	{
			switch(bd)
			{
			case BaseDialog::Close:
				ui.ButtonBox->setStandardButtons(QDialogButtonBox::Close);
				break;
			case BaseDialog::YesNo:
				ui.ButtonBox->setStandardButtons(QDialogButtonBox::Yes|QDialogButtonBox::No);
				break;
			case BaseDialog::NoButtons:
				ui.ButtonBox->setStandardButtons(QDialogButtonBox::NoButton);
				break;
			case BaseDialog::Apply:
				ui.ButtonBox->setStandardButtons(QDialogButtonBox::Apply);
				break;
			default:
				ui.ButtonBox->setStandardButtons(QDialogButtonBox::Ok|QDialogButtonBox::Cancel);
			}
		}
	}

	void BaseDialog::SetContentWidget(QWidget * widget)
	{
		this->contentWidget = widget;

		QGridLayout * grid = new QGridLayout(this->ui.frame_content);
		grid->addWidget(widget);

		//set dialogs size to widgets size
		this->resize( widget->size() );
	}

	QWidget * BaseDialog::getContent()
	{
		return this->contentWidget;
	}

	QPushButton* BaseDialog::AddApplyButton(const QObject * callback_receiver, const char * callback_method)
	{
		QPushButton * button = this->ui.ButtonBox->addButton(tr("Apply"), QDialogButtonBox::ApplyRole );
		QObject::connect(button, SIGNAL(clicked()), callback_receiver, callback_method);

		return button;
	}

	void BaseDialog::setAllowResizing( bool allowResizing )
	{
		if(!allowResizing) 
		{
			layout()->setSizeConstraint(QLayout::SetFixedSize);
			this->setSizeGripEnabled(false);
		} 
		else
		{
			layout()->setSizeConstraint(QLayout::SetDefaultConstraint);
			this->setSizeGripEnabled(true);
		}
	}

	bool BaseDialog::allowResizing() const
	{
		return this->isSizeGripEnabled();
	}

	void BaseDialog::keyPressEvent( QKeyEvent * e )
	{
		//avoid closing dialog on enter key pressed
		if(this->avoidCloseOnEnter && ( e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter) ) {
			return;
		}

		//call super in any other case
		QDialog::keyPressEvent(e);
	}

	void BaseDialog::setAvoidCloseOnEnter( bool avoidClosing )
	{
		this->avoidCloseOnEnter = avoidClosing;
	}

	bool BaseDialog::getAvoidCloseOnEnter() const
	{
		return this->avoidCloseOnEnter;
	}

	void BaseDialog::setInfoLineText(const QString & text)
	{
		QFontMetrics m(ui.textEdit_infoline->font());

		int textsWide = m.width(text);
		// with factor 2, because of font leading that is 0
		int rowHeight = m.lineSpacing()*2;
		// numb of rows is calculated going out from content widget size
		int numbRows = textsWide / contentWidget->size().width() 
			+ ((textsWide % contentWidget->size().width()>0)?1:0);
		ui.textEdit_infoline->setFixedHeight(numbRows * rowHeight);
		ui.textEdit_infoline->setVisible(true);
		ui.textEdit_infoline->setText(text);
	}



}//of namespace prova