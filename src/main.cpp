#include "mainwindow.h"
#include <QApplication>
#include <QFileDialog>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	//open xml file with data about users (holders), properties and treeItems 
	QString file = QFileDialog::getOpenFileName(0, "Select a data file",QDir::homePath(),"XML Files (*.xml)");
	if(!file.isNull())
	{
		//create application window
		MainWindow w(file.toUtf8().constData());
		w.show();
		return a.exec();
	}
	else
		return 0;

}
