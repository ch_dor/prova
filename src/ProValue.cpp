#include <ProDescription.h>
#include <ProValue.h>
#include <TreeElement.h>

using prova::TreeElementPtr;

namespace prova {

ProValue::ProValue()
{
}

ProValue::ProValue(PVALUE _value, ProDescriptionPtr _desc, TreeElementPtr _element)
	:value(_value),desc(_desc),linkedElement(_element)
{

}

ProValue::ProValue(const ProValue& other)
	:value(other.value),desc(other.desc),linkedElement(other.linkedElement)
{

}


ProValue::~ProValue()
{
}

ProValue::PVALUE ProValue::getValue() const
{ 
	return value;
}

ID ProValue::getDescId() const
{
	return desc->getPropertryID();
}

TreeElement::LinkId ProValue::getTreeElementId() const
{
	return linkedElement->getId();
}
void ProValue::setValue(PVALUE newvalue)
{
	switch(getDType())
	{
	case Bool:
		if(newvalue.type() == typeid(bool))
			value =  boost::get<bool>(newvalue);
		else
			throw std::exception("Conversion to boolean not possible");
		break;
	case Int:
		if(newvalue.type() == typeid(int))
			value =  boost::get<int>(newvalue);
		else
			throw std::exception( "Conversion to int not possible");
		break;
	case Long:
		if(newvalue.type() == typeid(long))
			value =  boost::get<long>(newvalue);
		else
			throw std::exception( "Conversion to long not possible");
		break;
	case Float:
		if(newvalue.type() == typeid(float))
			value =  boost::get<float>(newvalue);
		else
			throw std::exception( "Conversion to float not possible");
		break;
	case Double:
		if(newvalue.type() == typeid(double))
			value =  boost::get<double>(newvalue);
		else
			throw std::exception("Conversion to double not possible");
		break;
	case Text:
		if(newvalue.type() == typeid(std::string))
			value =  boost::get<std::string>(newvalue);
		else
			throw std::exception("Conversion to string not possible");
		break;
	default:
		throw std::exception("Conversion not possible");
		break;
	}
}

ProDescriptionPtr ProValue::getProDescription() const
{
	return desc;
}

TreeElementPtr ProValue::getTreeElement() const
{
	return linkedElement;
}

DType ProValue::getDType() const
{
	return desc->getDType();
}


}// namespace prova
