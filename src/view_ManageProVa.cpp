#include <common.h>
#include <Holder.h>
#include <view_ManageProVa.h>
#include <model_ManageProVa.h>
#include <ProDescription.h>
#include <boost/foreach.hpp>
#include <QComboBox>
#include <QSpinBox>
#include <QString>
#include <QLineEdit>
#include <QMessageBox>
#include <QCompleter>
#include <QRegExpValidator>
#include <QHeaderView>
#include <QEvent>
#include <QKeyEvent>
#include <QPainter>
#include <climits>
#include <cfloat>

using prova::ProVaModel;


namespace prova{

	
ProVaItemDelegate::ProVaItemDelegate(QObject *parent /*= 0*/)
	: QItemDelegate(parent)
{

}

QWidget * ProVaItemDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	if(index.column()==0)
	{
		QLineEdit * name = new QLineEdit(parent);
		NameValidator *nameVal = new NameValidator(parent);
		name->setValidator(nameVal);
		return name;
	}
	else if(index.column()==1)
	{
		QComboBox *datatype = new QComboBox(parent);
		datatype->setEditable(false);
		QStringList dtypes;
		dtypes << "bool" << "int" << "float" << "double" << "text";
		datatype->addItems(dtypes);
		datatype->installEventFilter(const_cast<ProVaItemDelegate*>(this));
		return datatype;
	}
	else if(index.column()==2)
	{
		const ProVaViewFilterProxyModel* tmp = static_cast<const ProVaViewFilterProxyModel*>(index.model());
		if(tmp)
		{
			QModelIndex realItemIndex = tmp->mapToSource(index);
			const ProVaModel* model = static_cast<const ProVaModel*>(tmp->sourceModel());
			QWidget* editor;
			if(model->getManageKind() == ProVaModel::Values)
			{
				switch(ProDescription::PropertyType2DType(model->getItemDatatype(realItemIndex)))
				{
					case Double: 
						editor = new ProVaDoubleSpinBox(parent);
						((ProVaDoubleSpinBox*)editor)->setValue(model->data(realItemIndex,Qt::EditRole).toDouble());
						break;
					case Float:
						editor = new ProVaDoubleSpinBox(parent,true);
						((ProVaDoubleSpinBox*)editor)->setValue(model->data(realItemIndex,Qt::EditRole).toFloat());
						break;
					case Int:
						editor = new ProVaIntSpinBox(parent);
						((ProVaIntSpinBox*)editor)->setValue(model->data(realItemIndex,Qt::EditRole).toInt());
						break;
					case Bool:
						{
							editor = new ProVaLineEdit(parent,true);
							((ProVaLineEdit*)editor)->setText(model->data(realItemIndex,Qt::EditRole).toString());
						}
						break;
					case Text:
						{
							editor = new ProVaLineEdit(parent);
							((ProVaLineEdit*)editor)->setText(model->data(realItemIndex,Qt::EditRole).toString());
						}
						break;

					default:
						editor = QItemDelegate::createEditor(parent, option, realItemIndex);
				}
			}
			else
			{
				QComboBox *owner = new QComboBox(parent);
				owner->setEditable(false);
				ManageProVaView* view = static_cast<ManageProVaView*>(this->parent());
				owner->addItems(view->getHolderList());
				owner->installEventFilter(const_cast<ProVaItemDelegate*>(this));
				editor = owner;
			}
			return editor;
		}
	}
	return QItemDelegate::createEditor(parent, option, index);
}


void ProVaItemDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
	const ProVaViewFilterProxyModel* tmp = static_cast<const ProVaViewFilterProxyModel*>(index.model());
	const ProVaModel* model = static_cast<const ProVaModel*>(tmp->sourceModel());
	QModelIndex realItemIndex = tmp->mapToSource(index);
	if(model && index.column()==1)
	{
		QComboBox* datatype = static_cast<QComboBox*>(editor);
		if(datatype) 
		{
			datatype->setCurrentIndex(0);
		}
	}
	else if(model && index.column()==2)
	{
		if(model->getManageKind()==ProVaModel::Properties)
		{
			QComboBox* owner = static_cast<QComboBox*>(editor);
			std::string own = model->getItemHolder(realItemIndex);
			ManageProVaView* view = static_cast<ManageProVaView*>(this->parent());
			int indexof = view->getHolderList().indexOf(own.c_str());
			if(owner) 
			{
				(indexof!=-1)?owner->setCurrentIndex(indexof):owner->setCurrentIndex(0);
			}
		}
	}
	else 
	{
		QItemDelegate::setEditorData(editor, index);
	}
}

void ProVaItemDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
	const ProVaViewFilterProxyModel* tmp = static_cast<const ProVaViewFilterProxyModel*>(index.model());

	if(!tmp)
	{
		QItemDelegate::setModelData(editor, model, index);
		return;
	}


	QModelIndex realItemIndex = tmp->mapToSource(index);
	ProVaModel* m = static_cast<ProVaModel*>(tmp->sourceModel());
	if(!m)
	{
		QItemDelegate::setModelData(editor, model, index);
		return;
	}

	if(index.column() == 0)
	{
		QLineEdit* textbox = static_cast<QLineEdit*>(editor);
		if(textbox)
		{
			if(!textbox->text().isEmpty())
				m->setData(realItemIndex,textbox->text(),Qt::EditRole);
		}
	}
	else if(index.column() == 1)
	{
		QComboBox* datatype = static_cast<QComboBox*>(editor);
		if(datatype)
		{
			m->setData(realItemIndex,datatype->currentText(),Qt::EditRole);
		}
	}
	else if(index.column() == 2 && m->getManageKind()==ProVaModel::Values)
	{
		bool EmptyValueInEditor = false;
		switch(ProDescription::PropertyType2DType(m->getItemDatatype(realItemIndex)))
		{
			case Int:
				{
					ProVaIntSpinBox* spe = (ProVaIntSpinBox*)editor;
					if(spe && spe->getIsEmpty())
					{
						EmptyValueInEditor=true;
					}
					break;
				}
			case Float:
			case Double:
				{
					ProVaDoubleSpinBox* spe = (ProVaDoubleSpinBox*)editor;
					if(spe && spe->getIsEmpty())
					{
						EmptyValueInEditor=true;
					}
					break;
				}
			case Bool:
				{
					ProVaLineEdit* be = (ProVaLineEdit*)editor;
					if(be && be->getIsEmpty())
					{
						m->setData(realItemIndex,QVariant(),Qt::EditRole);
						return;
					}
					if(be && !be->getIsEmpty())
					{
						m->setData(realItemIndex,QVariant(be->getBooleanValue()),Qt::EditRole);
						return;
					}
					break;
				}
			case Text:
				{
					ProVaLineEdit* be = (ProVaLineEdit*)editor;
					if(be && be->getIsEmpty())
					{
						EmptyValueInEditor=true;
					}
					break;
				}
		}
		if(EmptyValueInEditor)
		{
			m->setData(realItemIndex,QVariant(),Qt::EditRole);
		}
		else
		{
			QItemDelegate::setModelData(editor, model, index);
		}

	}
	else if(index.column() == 2 && m->getManageKind()==ProVaModel::Properties)
	{
		QComboBox* owner = static_cast<QComboBox*>(editor);
		if(owner)
		{
			m->setData(realItemIndex,owner->currentIndex(),Qt::EditRole);
		}

	}
	else 
	{
		QItemDelegate::setModelData(editor, model, index);
	}

}

void ProVaItemDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
	 editor->setGeometry(option.rect);
}


/*----------------------------------------------------------------------------------------------------------------*/

ManageProVaView::ManageProVaView( QWidget *parent)
	: QTableView(parent)
{
	this->verticalHeader()->hide();
	this->setSelectionMode (QAbstractItemView::SingleSelection);
	this->setSelectionBehavior(QAbstractItemView::SelectRows);
	this->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
	owners = QStringList();
}

void ManageProVaView::setModel(QAbstractItemModel *model)
{
	QTableView::setModel(model);
	this->resizeColumnsToContents();
	owners.clear();
	ProVaViewFilterProxyModel* tmp_proxymodel = static_cast<ProVaViewFilterProxyModel*>(model);
	ProVaModel* tmp_sourcemodel = static_cast<ProVaModel*>(tmp_proxymodel->sourceModel());
	BOOST_FOREACH(HolderPtr own, *tmp_sourcemodel->getHolderList())
	{
		owners << own->getHolderName().c_str();
	}
}

QStringList ManageProVaView::getHolderList() const
{
	return owners;
}



/*----------------------------------------------------------------------------------------------------------------*/

ProVaIntSpinBox::ProVaIntSpinBox(QWidget *p /*= 0*/)
	: QSpinBox(p) 
{
	isEmpty = true;
	setRange(-INT_MAX,INT_MAX);
	connect(this->lineEdit(),SIGNAL(textChanged(QString)),this,SLOT(checkIfEmpty(QString)));
	connect(this,SIGNAL(valueChanged(int)),this,SLOT(checkIfEmpty(int)));
}

bool ProVaIntSpinBox::getIsEmpty() const
{
	return isEmpty;
}

void ProVaIntSpinBox::setIsEmpty(const bool& ie)
{
	isEmpty = ie;
}

void ProVaIntSpinBox::checkIfEmpty(const QString& text)
{
	setIsEmpty(text.isEmpty());
}

void ProVaIntSpinBox::checkIfEmpty(const int& v)
{
	setIsEmpty(false);
}

/*----------------------------------------------------------------------------------------------------------------*/

ProVaDoubleSpinBox::ProVaDoubleSpinBox(QWidget *p /*= 0*/, bool _isFloat/*=false*/)
	: QDoubleSpinBox(p) 
{
	if(!_isFloat){
		setDecimals(DBL_DIG);
		setRange(-DBL_MAX,DBL_MAX);
	}
	else{
		setDecimals(FLT_DIG);
		setRange(-FLT_MAX,FLT_MAX);
	}
	setSingleStep(1/10.0);
	isEmpty = true;

	connect(this->lineEdit(),SIGNAL(textChanged(QString)),this,SLOT(checkIfEmpty(QString)));
	connect(this,SIGNAL(valueChanged(double)),this,SLOT(checkIfEmpty(double)));
}

bool ProVaDoubleSpinBox::getIsEmpty() const
{
	return isEmpty;
}

void ProVaDoubleSpinBox::setIsEmpty(const bool& ie)
{
	isEmpty = ie;

}

void ProVaDoubleSpinBox::checkIfEmpty(const QString& text)
{
	setIsEmpty(text.isEmpty());
}

void ProVaDoubleSpinBox::checkIfEmpty(const double& v)
{
	setIsEmpty(false);
}

/*----------------------------------------------------------------------------------------------------------------*/

ProVaLineEdit::ProVaLineEdit(QWidget *p /*= 0*/, bool isBoolean /*= false*/) :QLineEdit(p)
{
	isEmpty = true;
	if(isBoolean)
	{
		QRegExpValidator *boolVal = new QRegExpValidator(
			QRegExp("(t(rue|)|f(alse|)|T(RUE|)|F(ALSE|)|0|1|y(es|)|n(o|)|Y(ES|)|N(O|))"), 
			p);
		setValidator(boolVal);
		QStringList vn_list;
		vn_list << "false" << "true" << "0" << "1" << "yes" << "no";
		QCompleter* completer = new QCompleter(vn_list);
		completer->setCompletionMode(QCompleter::InlineCompletion);
		completer->setCaseSensitivity(Qt::CaseInsensitive);
		setCompleter(completer);
		connect(this,SIGNAL(textChanged(QString)),this,SLOT(setBooleanValue(QString)));
	}
	connect(this,SIGNAL(textChanged(QString)),this,SLOT(checkIfEmpty(QString)));
}

bool ProVaLineEdit::getIsEmpty() const
{
	return isEmpty;
}

void ProVaLineEdit::setBooleanValue(QString text) 
{
	if(text.contains("f",Qt::CaseInsensitive) 
		|| text.contains("n",Qt::CaseInsensitive) 
		|| text.contains("0"))
		value = false;
	else
		value = true;
}

void ProVaLineEdit::setIsEmpty(const bool& ie)
{
	isEmpty = ie;

}

void ProVaLineEdit::checkIfEmpty(const QString& text)
{
	setIsEmpty(text.isEmpty());
}

bool ProVaLineEdit::getBooleanValue() const
{
	return value;
}


NameValidator::NameValidator(QObject* parent /*= 0*/)
	:QRegExpValidator(QRegExp("[0-9/a-z/A-Z/_/#/$/%]*"),parent)
{
}

QValidator::State NameValidator::validate(QString& input, int& pos) const
{
	if(input.isEmpty())
		return QValidator::Acceptable;
	return QRegExpValidator::validate(input,pos);
}

}


