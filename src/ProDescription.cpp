#include <common.h>
#include <ProDescription.h>
#include <Holder.h>

namespace prova {


ProDescription::ProDescription(void)
	:propertyname(""),dtype(prova::Unkown),propertyid(-1) 
{
}

ProDescription::ProDescription(const std::string& _name, DType _dtype, HolderPtr _owner, ID _id)
	:propertyname(_name),dtype(_dtype),propertyid(_id) 
{
	if(_owner)
		owner =_owner;
}

ProDescription::ProDescription(const ProDescription& other)
	:propertyname(other.propertyname),dtype(other.dtype),propertyid(other.propertyid)
{
	if(other.owner)
		owner =other.owner;
}


ProDescription::~ProDescription(void)
{
}

DType ProDescription::getDType() const
{
	return dtype;
}

std::string ProDescription::getDTypeAsString() const
{
	return PropertyType2Str(dtype);
}

std::string ProDescription::PropertyType2Str(const DType & type) 
{//("type" in ('bool', 'int', 'float', 'double', 'text', 'date')) in all dbms

	switch(type)
	{
	case Bool:
		return "bool";
	case Int:
		return "int";
	case Long:
		return "long";
	case Float:
		return "float";
	case Double:
		return"double";
	case Text:
		return "text";
	default:
		throw std::exception("Type unknown.");
	}
}

DType ProDescription::PropertyType2DType(const std::string & type)
{
	if (type == "bool")
		return Bool;
	if (type == "int")
		return Int;
	if (type == "long")
		return Long;
	if (type == "float")
		return Float;
	if (type == "double")
		return Double;
	if (type == "text")
		return Text;

	return Unkown;
}

void ProDescription::setPropertyID(ID _id)
{
	propertyid = _id;
}

ID ProDescription::getPropertryID() const
{
	return propertyid;
}

bool ProDescription::operator==(const ProDescription& other) const
{
	if(propertyid!=other.propertyid)
		return false;	
	if(propertyname != other.propertyname)
		return false;
	if(dtype!=other.dtype)
		return false;
	if(!((*owner) ==(*other.owner)))
		return false;
	return true;

}

std::string ProDescription::getPropertyName() const
{
	return propertyname;
}

HolderPtr ProDescription::getHolder() const
{
	return owner;
}

void ProDescription::setPropertyName(std::string& newname)
{
	propertyname = newname;
}

}// namespace prova
