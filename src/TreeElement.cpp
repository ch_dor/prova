#include <TreeElement.h>
#include <Holder.h>
namespace prova {

prova::ID TreeElement::getHolderId() const
{
	return holderId;
}

int TreeElement::getLevel() const
{
	return level;
}

std::string TreeElement::getLabel() const
{
	return label;
}

TreeElement::TreeElement(int _id, int _level, std::string& _label, int _holderId, prova::ID _parentId)
	: id(_id),level(_level),label(_label),holderId(_holderId), parentId(_parentId)
{

}

TreeElement::TreeElement()
	:id(-1),level(-1),label(""),holderId(-1),parentId(-1)
{
}

TreeElement::~TreeElement(void)
{
}

prova::ID TreeElement::getParentId() const
{
	return parentId;
}

prova::TreeElement::LinkId prova::TreeElement::getId() const
{
	return id;
}

}