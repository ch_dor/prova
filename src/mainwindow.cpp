#include "mainwindow.h"
#include "common.h"
#include "ui_mainwindow.h"
#include <dialog_Manageprova.h>
#include <model_Manageprova.h>
#include <Holder.h>
#include <ProDescription.h>
#include <TreeElement.h>
#include "boost/date_time/posix_time/posix_time.hpp"
#include <iostream> 
#include <sstream> 
#include <fstream>
#include <ostream> 
#include <boost/archive/xml_oarchive.hpp> 
#include <boost/archive/xml_iarchive.hpp> 
#include <boost/serialization/shared_ptr.hpp>
#include <boost/archive/archive_exception.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <QStandardItemModel>

using namespace boost::posix_time;
using namespace boost::gregorian;

MainWindow::MainWindow(const std::string datafile, QWidget *parent) :
    QMainWindow(parent),
	datafile(datafile),
	dlg(NULL),
    ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	ui->ValuesTreeView->setSelectionBehavior(QAbstractItemView::SelectRows);
	ui->ValuesTreeView->setContextMenuPolicy(Qt::CustomContextMenu);
	connect(ui->ValuesTreeView, SIGNAL(customContextMenuRequested(const QPoint &)), this, SLOT(showContextMenu(const QPoint &)));
	contextMenu = new QMenu(ui->ValuesTreeView);
	contextMenu->addAction("Edit property values", this, SLOT(showEditPropertyValueDialog()));
	model = new QStandardItemModel(this);

	//read model and tree items from file
	boost::filesystem::path file = boost::filesystem::system_complete(datafile);
	std::ifstream ifs;
	std::ofstream ofs;
	if( boost::filesystem::exists(file) )
	{
		ifs.open(file.string()/*, std::ifstream::in*/);
		if (!ifs.is_open()) {
			throw std::exception("File not found");
		}

	}
	loadModel(ifs,m);
	ifs.close();

	CreateTree();
	ui->ValuesTreeView->setModel(model);
}

/*
*	Build a tree of Tree Elements from xml file
*   Three levels are supported
*/
void MainWindow::CreateTree()
{
	model->setColumnCount(1);
	model->setHorizontalHeaderLabels(QStringList()<<"Manage tree item values");
	// temporary list of parents for next level and actual level items
	QList<QStandardItem*> tmpactuallevelitems; 
	QList<QStandardItem*> tmpparentitems; 
	std::vector<prova::TreeElementPtr> tmpparentelements;
	std::vector<prova::TreeElementPtr> tmpactuallevelelements;
	//iterate supported number of levels
	for(int level=0;level<3;level++)
	{
		// row important for first level items, that have to be added directly to QStandardItemModel
		int row=0;
		BOOST_FOREACH(prova::TreeElementPtr element, treeitems)
		{
			// check if element belongs to actual level, otherwise skip
			if(element->getLevel() == level)
			{
				// create QStandardItem (TreeItem) from TreeElement
				QStandardItem* tmp = new QStandardItem(element->getLabel().c_str());
				// add item and corresponding element to actual level lists
				tmpactuallevelitems.append(tmp);
				tmpactuallevelelements.push_back(element);
				// if root level, append items directly to model
				if(level==0) 
				{
					model->setItem(row,0,tmp);
					row++;
				}
				else 
				{
					// look in the list of parents for the parent for actual element 
					for(int i=0;i<(int)tmpparentelements.size();++i)
					{
						if(element->getParentId()==tmpparentelements[i]->getId())
						{
							// append child row to corresponding parent element
							tmpparentitems[i]->appendRow(tmp);
							break;
						}
					}
				}
			}
		}
		// clear old parents
		tmpparentelements.clear();
		tmpparentitems.clear();
		// remove actual level items to parents for next level
		tmpparentelements=tmpactuallevelelements;
		tmpparentitems=tmpactuallevelitems;
		//clear actual items containers
		tmpactuallevelelements.clear();
		tmpactuallevelitems.clear();
	}
}

void MainWindow::showContextMenu(const QPoint & p)
{
	QModelIndex index = ui->ValuesTreeView->indexAt(p);
	
	if (index.isValid())
	{
		actualidx = index;
		contextMenu->exec(ui->ValuesTreeView->mapToGlobal(p));

	}    

}

MainWindow::~MainWindow()
{
	delete ui;
}

/**
	* starts dialog to edit existing or add new properties
	*/
void MainWindow::StartManageProperties()
{
	dlg = new prova::ManagePropertyDescriptionsDialog(this,m);
	dlg->setAttribute( Qt::WA_DeleteOnClose );
	dlg->exec();
}

/**
* if dialog is closed, store last state into xml file 
*/
void MainWindow::closeEvent(QCloseEvent *event)
{
	boost::filesystem::path file = boost::filesystem::system_complete(datafile);
	std::ofstream ofs;
	ofs.open(file.string());
	saveModel(ofs,m);
	event->accept();
}

/**
* serialize model and tree items 
*/
void MainWindow::saveModel(std::ofstream& ss, prova::ProVaModelPtr model) 
{ 
	boost::archive::xml_oarchive oa(ss);
	oa << boost::serialization::make_nvp("TreeItems",treeitems);
	oa << boost::serialization::make_nvp("Model",model); 
	ss.close();
} 


/**
* load model from xml file or work with static dummy data 
*/
void MainWindow::loadModel(std::ifstream& ss, prova::ProVaModelPtr& m)
{ 
	//if data file not found or empty
	//work with dummy data
	if(!ss || ss.peek() == std::ifstream::traits_type::eof())
	{
		ptime now = second_clock::local_time();
		prova::HolderPtr h1(new prova::Holder(1,prova::Holder::User,"admin",to_simple_string(now)));
		prova::HolderPtr h2(new prova::Holder(22,prova::Holder::User,"test_user","1953-09-23 10:15:24"));
		prova::HolderPtr h3(new prova::Holder(2,prova::Holder::Group,"user_pool","1981-08-20 08:05:00"));
		prova::HolderListPtr list = boost::shared_ptr<prova::HolderList>(new prova::HolderList() );
		list->push_back(h1);list->push_back(h2);list->push_back(h3);
		prova::ProDescriptionPtr pd1(new prova::ProDescription("Test1",prova::Int,h1,123));
		prova::ProDescriptionPtr pd2(new prova::ProDescription("Test2",prova::Double,h3,12));
		prova::ProDescriptionPtr pd3(new prova::ProDescription("Test3",prova::Float,h3,1));
		prova::ProDescriptionPtr pd4(new prova::ProDescription("Textproperty",prova::Text,h2,345));
		prova::ProDescriptionListPtr d = boost::shared_ptr<prova::ProDescriptionList>(new prova::ProDescriptionList() );
		d->push_back(pd1);d->push_back(pd2);d->push_back(pd3);d->push_back(pd4);
		m = boost::shared_ptr<prova::ProVaModel>(new prova::ProVaModel(list,d));

		//create dummy data items for dummy tree in the main window
		prova::TreeElementPtr parent_level0 = boost::shared_ptr<prova::TreeElement>(new prova::TreeElement(1,0,std::string("Development Tools"),1));
		treeitems.push_back(parent_level0);
		prova::TreeElementPtr parent_level1 = boost::shared_ptr<prova::TreeElement>(new prova::TreeElement(2,1,std::string("Qt Creator"),1,parent_level0->getId()));
		treeitems.push_back(parent_level1);
		treeitems.push_back(boost::shared_ptr<prova::TreeElement>(new prova::TreeElement(3,2,std::string("Qt Designer"),1,parent_level1->getId())));
		treeitems.push_back(boost::shared_ptr<prova::TreeElement>(new prova::TreeElement(4,2,std::string("qmake"),1,parent_level1->getId())));
		treeitems.push_back(boost::shared_ptr<prova::TreeElement>(new prova::TreeElement(5,2,std::string("Qt Linguist"),1,parent_level1->getId())));
		treeitems.push_back(boost::shared_ptr<prova::TreeElement>(new prova::TreeElement(6,2,std::string("Qt Assistant"),1,parent_level1->getId())));
		treeitems.push_back(boost::shared_ptr<prova::TreeElement>(new prova::TreeElement(7,1,std::string("Meta-Object Compiler"),1,parent_level0->getId())));
		treeitems.push_back(boost::shared_ptr<prova::TreeElement>(new prova::TreeElement(8,0,std::string("User Interfaces"),1)));
	}
	else
	{
		boost::archive::xml_iarchive ia(ss); 
		ia >> boost::serialization::make_nvp("TreeItems",treeitems); 
		ia >> boost::serialization::make_nvp("Model",m); 
	}
}
/**
*    manage values, owned by a tree item                                                                  
**/
void MainWindow::showEditPropertyValueDialog()
{
	QStandardItem *selectedItem =model->itemFromIndex(actualidx);
	// find out which tree element was clicked via context menu
	BOOST_FOREACH(prova::TreeElementPtr element, treeitems)
	{
		if(selectedItem->text() == QString(element->getLabel().c_str()))
		{
			dlg = new prova::ManagePropertyDescriptionsDialog(this,m,element);
			dlg->setAttribute( Qt::WA_DeleteOnClose );
			dlg->exec();
			break;
		}
	}

}





