#include <Holder.h>
#include <TreeElement.h>
#include <dialog_ManageProVa.h>
#include <model_ManageProVa.h>
#include <boost/algorithm/string.hpp>
#include <QMessageBox>
#include <QSortFilterProxyModel>
#include <QScrollBar>

namespace prova{


ManagePropertyDescriptionsWidget::ManagePropertyDescriptionsWidget(ProVaModelPtr _model, QWidget *parent /*= 0*/
	,TreeElementPtr le /*= boost::shared_ptr<TreeElement>()*/)
	:model(_model)
{
	ui.setupUi(this);
	// if it is a widget to manage values
	if(le)
	{
		model->setTreeElement(le);
		//resolve owner
		HolderListPtr onwers = model->getHolderList();
		// check if owner of tree item occures in the list of owners from model
		HolderList::const_iterator pos = GetHolderById( *onwers, le->getHolderId());
		if(pos == onwers->end()) {
			throw std::exception("Model corruption. Holder for Element not found" );
		}
		//create tool tip text
		QString tmp = QString("Assigned property values for element %1, owned by user %2").arg(le->getLabel().c_str(),(*pos)->getHolderName().c_str());
		ui.actualPropertyValue->setText(tmp);
		ui.actualPropertyValue->setVisible(true);
		ui.btn_addProp->setVisible(false);
		ui.btn_addProp->setEnabled(false);
		ui.btn_removeProp->setText("Remove selected value");
		ui.checkbox_showAll->setText("Show empty values ");
		ui.checkBox_showInherited->setVisible(true);
		if(le->getLevel()==0){
			// no inherited values for root possible
			ui.checkBox_showInherited->setEnabled(false);
		}
		else {
			// show values inherited from parent tree items by default
			ui.checkBox_showInherited->setEnabled(true);
		}

	}
	// if widget to manage properties
	else
	{
		model->clearPropertyValues();
		model->setManageKind(ProVaModel::Properties);
		ui.actualPropertyValue->setVisible(false);
		ui.checkbox_showAll->setVisible(false);
		ui.checkbox_showAll->setEnabled(false);
		ui.checkBox_showInherited->setVisible(false);
	}
	// create proxy model based on QAbstractTableModel to enable sorting
	ProVaViewFilterProxyModel *proxyModel = new ProVaViewFilterProxyModel(this);
	proxyModel->setSourceModel(model.get());
	// use proxy model as QAbstractItemModel in the table view
	ui.tableView->setModel(proxyModel);
	ui.tableView->setSortingEnabled(true);

	// QItemDelegate provides display and editing facilities for data items from a model
	ProVaItemDelegate* del = new ProVaItemDelegate(ui.tableView);
	ui.tableView->setItemDelegate(del);
	//select first row in the table view by default
	//selecting lyne by line
	ui.tableView->selectionModel()->select(ui.tableView->model()->index(0,0), 
		QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
	UpdateView();

}

void ManagePropertyDescriptionsWidget::CreateNewPropDesc()
{
	int rowsnumb = ui.tableView->model()->rowCount();
	ui.tableView->model()->insertRow(rowsnumb);
	ui.tableView->scrollToBottom();
}

void ManagePropertyDescriptionsWidget::RemoveItem()
{
	QItemSelectionModel *select = ui.tableView->selectionModel();
	if(select->hasSelection())
	{
		ProVaViewFilterProxyModel* tmp = (ProVaViewFilterProxyModel*)ui.tableView->model();
		ProVaModel* m = (ProVaModel*)(tmp->sourceModel());
		QModelIndex realIndex = tmp->mapToSource(select->selectedRows()[0]);
		if(!m->isEmptyItem(realIndex))
		{
			if(m->getManageKind() == ProVaModel::Properties)
			{
				int result = QMessageBox::warning(this, tr("Delete property description"),
					QString("\nDeleting of a property description causes \nremoving of corresponding property values\n"
					"Are you sure to remove property %1?\n").arg(m->getItemName(realIndex).c_str()),
					tr("No"), tr("Yes, remove property including its property values"));
				if(result != 0){
					m->removeRow(realIndex.row());
				}
			}
			else if(m->getManageKind() == ProVaModel::Values && m->getItemIsEditable(realIndex))
			{
				QModelIndex tmp = m->index(realIndex.row(),2);
				m->setData(tmp,QVariant(),Qt::EditRole);
			}
		}
		else
		{
			m->removeRow(realIndex.row());
		}
		ui.tableView->setFocus(Qt::OtherFocusReason);
	}

}

void ManagePropertyDescriptionsWidget::ShowEmptyValues()
{
	((ProVaViewFilterProxyModel*)ui.tableView->model())->setShowEmptyValues(ui.checkbox_showAll->isChecked());
	((ProVaViewFilterProxyModel*)ui.tableView->model())->invalidate();
	ui.tableView->selectionModel()->select(ui.tableView->model()->index(0,0), 
		QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
	UpdateView();
}

void ManagePropertyDescriptionsWidget::ShowInheritedValues()
{
	((ProVaViewFilterProxyModel*)ui.tableView->model())->setShowInheritedValues(ui.checkBox_showInherited->isChecked());
	((ProVaViewFilterProxyModel*)ui.tableView->model())->invalidate();
	ui.tableView->selectionModel()->select(ui.tableView->model()->index(0,0), 
		QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
	UpdateView();
}

void ManagePropertyDescriptionsWidget::UpdateView()
{
	ProVaViewFilterProxyModel* proxymodel = (ProVaViewFilterProxyModel*)ui.tableView->model();
	ui.btn_removeProp->setEnabled(true);
	//show vertical scrollbar if more than 10 rows (properties or values) exist
	//otherwise adatp the size of content widget and of the dialog
	int nNumRows = proxymodel->rowCount();
	if(nNumRows>10) 
		nNumRows=10; 
	int nRowHeight = ui.tableView->rowHeight(0);
	int nTableHeight = 
		(nNumRows * nRowHeight) + 
		ui.tableView->horizontalHeader()->height() +
		2 * ui.tableView->frameWidth();
	int nTableWidth = 
		ui.tableView->columnWidth(0)+ui.tableView->columnWidth(1)+ui.tableView->columnWidth(2) + 2 * ui.tableView->frameWidth();
	if(ui.tableView->verticalScrollBar()->isVisible()){
		nTableWidth += ui.tableView->style()->pixelMetric(QStyle::PM_ScrollBarExtent);
	}
	
	QSize newsize(nTableWidth,nTableHeight);
	ui.tableView->setMinimumSize(newsize);

	if(proxymodel->rowCount()>0)
	{
		ProVaModel* m = (ProVaModel*)(proxymodel->sourceModel());
		QItemSelectionModel *selection = ui.tableView->selectionModel();
		if(selection->hasSelection())
		{
			QItemSelection is = selection->selection();
			QModelIndex selectedIndex = is.indexes()[0];
			// compute real index for the data model
			QModelIndex realIndex = proxymodel->mapToSource(selectedIndex);
			ui.tableView->setFocus(Qt::OtherFocusReason);
			// if managing of values, check if value removable (not a value from parent)
			if(!m->isEmptyItem(realIndex) && m->getManageKind() == ProVaModel::Values)
				if(!m->getItemIsEditable(realIndex) || !m->itemHasAssignedValue(realIndex.row()))
				{
					ui.btn_removeProp->setEnabled(false);
				}
		}
		else{
		//nothing to remove, because no selection
			ui.btn_removeProp->setEnabled(false);
		}
	}
	else {
	// nothing to remove
		ui.btn_removeProp->setEnabled(false);
	}

}

void ManagePropertyDescriptionsWidget::removeEmptyPropertyDescriptions()
{
	((ProVaViewFilterProxyModel*)ui.tableView->model())->removeEmptyPropertyDescriptions();
}

void ManagePropertyDescriptionsWidget::OnItemClicked()
{
	UpdateView();
}

ManagePropertyDescriptionsWidget::~ManagePropertyDescriptionsWidget()
{
	removeEmptyPropertyDescriptions();
}

void ManagePropertyDescriptionsWidget::setTreeElement(const TreeElementPtr le)
{
	if(le)
	{
		model->setTreeElement(le);
		//resolve owner
		HolderListPtr onwers = model->getHolderList();
		HolderList::const_iterator pos = GetHolderById( *onwers, le->getHolderId() );
		if(pos == onwers->end()) {
			throw std::exception( "Model corruption. Holder for MoMa Element not found" );
		}
		// create tool tip text
		QString tmp = QString("Assigned property values for element %1, owned by user %2").arg(le->getLabel().c_str(),(*pos)->getHolderName().c_str());
		ui.actualPropertyValue->setText(tmp);
		ui.tableView->selectRow(0);
		// for root element no inherited values possible
		if(le->getLevel()==0){
			ui.checkBox_showInherited->setChecked(false);
			ui.checkBox_showInherited->setEnabled(false);
		}
		else {
			ui.checkBox_showInherited->setEnabled(true);
		}
		((ProVaViewFilterProxyModel*)ui.tableView->model())->invalidate();
		//select first row in the table view by default
		//selecting line by line
		ui.tableView->selectionModel()->select(ui.tableView->model()->index(0,0), 
		QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
		UpdateView();
	}
}


//------------------------------------------------------------------------------
ManagePropertyDescriptionsDialog::ManagePropertyDescriptionsDialog(QWidget *parent, ProVaModelPtr model, TreeElementPtr le)
 : BaseDialog(parent,BaseDialog::Close)
{
	ManagePropertyDescriptionsWidget* contentwidget;
	// set title depending on the kind of dialog
	if(le)
	{
		this->setWindowTitle(tr("Manage property values"));
		contentwidget = new ManagePropertyDescriptionsWidget(model, this,le);
		setModal(false);
	}
	else
	{
		this->setWindowTitle(tr("Manage property descriptions"));
		contentwidget = new ManagePropertyDescriptionsWidget(model, this);
		setModal(true);
	}
	SetContentWidget(contentwidget);
	setAttribute(Qt::WA_DeleteOnClose);
	setAllowResizing(true);


}

void ManagePropertyDescriptionsDialog::setTreeElement(const TreeElementPtr le)
{
	ManagePropertyDescriptionsWidget* tmp = dynamic_cast<ManagePropertyDescriptionsWidget*>(this->contentWidget);
	if(tmp)
		tmp->setTreeElement(le);
}



//------------------------------------------------------------------------------

}//of namespace



